<?php
$cache_expire = 60 * 60 * 24 * 365;
header("Pragma: public");
header("Cache-Control: max-age=" . $cache_expire);
header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $cache_expire) . ' GMT');
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>oaccount/merge</title>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<!--  <script src="//connect.facebook.net/en_US/all.js"></script>-->
</head>
<body>
<a href="#" class="connect">connect</a>
<div id="fb-root"></div>
<script type="text/javascript">

  window.fbAsyncInit = function () {
    FB.init({
      appId : '334841439876801', // App ID
      // channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
      status: true, // check login status
      cookie: true, // enable cookies to allow the server to access the session
      oauth : true, // enable OAuth 2.0
      xfbml : true  // parse XFBML
    });

    //
    // All your canvas and getLogin stuff here
    //
  };

  // Load the SDK Asynchronously
  (function (d) {
    var js, id = 'facebook-jssdk';
    if (d.getElementById(id)) {
      return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    d.getElementsByTagName('head')[0].appendChild(js);
  }(document));

  $(function () {

    $('a.connect').click(function () {

      FB.login(function (response) {
        if (response.authResponse) {
          console.log('Welcome!  Fetching your information.... ');
          FB.api('/me', function (response) {
            console.log('Good to see you, ' + response.name + '.');
            console.log('data', response);
          });
        } else {
          console.log('User cancelled login or did not fully authorize.');
        }
      });

      return false;

    });
  });

</script>
</body>
</html>