/**
 * Получаем все теги по идшнику картинки
 *
 * @param string $photo_id
 */
keyword[/get]

/**
 * Количество тегов для картинки.
 *
 * @param string $photo_id
 */
keyword/count

/**
 * Создаем новый кейворд.
 *
 * @return array|NULL
 * @throws DbError|Exception
 */
keyword/create

/**
 * Удаляем кейворд переданный в параметре.
 *
 * @return array|null
 */
keyword/delete

/**
 * Устанавливаем снимаем флаг bad с тега.
 *
 * @param string $keyword
 * @param string $value
 * @return bool|int
 */
keyword/bad