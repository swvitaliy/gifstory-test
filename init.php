<?php

if (!defined('APITEST_INIT')) {
    define('APITEST_INIT', '1');

    define('ROOT_DIR', __DIR__);
    define('LIB_DIR', __DIR__ . '/_lib');
    define('APIBASE_URL', 'http://api3.gifstory.co/1.2/');
    define('GIFSTRY_TEST_USER_AGENT', 'GIFSTRY_TEST_USER_AGENT');

    require LIB_DIR . '/curl.php';
    require LIB_DIR . '/ftoken.php';
    require LIB_DIR . '/param.php';
}
