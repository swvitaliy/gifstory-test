<?php

set_time_limit(300);
ini_set('max_execution_time', 300);
ini_set('display_errors', 1);
error_reporting(
    E_ALL
    // 0
    // E_ALL | E_STRICT
);

$app_id = "334841439876801";
$app_secret = "83d77262dea434401f145d2479c87dc7";
$my_url = "http://dev.webparadox.com/vit/gifstory-api/tests/utils/fb_conn1.php";

session_start();

$code = $_REQUEST["code"];

if (empty($code))
{
    $_SESSION['state'] = md5(uniqid(rand(), TRUE)); // CSRF protection
    $dialog_url = "https://www.facebook.com/dialog/oauth?client_id="
            . $app_id . "&redirect_uri=" . urlencode($my_url) . "&state="
            . $_SESSION['state'] . "&scope=user_birthday,read_stream";

    echo("<script> top.location.href='" . $dialog_url . "'</script>");
}

if (empty($_REQUEST['error']))
{

    if ($_SESSION['state'] && ($_SESSION['state'] === $_REQUEST['state']))
    {
        $token_url = "https://graph.facebook.com/oauth/access_token?"
                . "client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url)
                . "&client_secret=" . $app_secret . "&code=" . $code;

        $response = file_get_contents($token_url);
        $params = null;
        parse_str($response, $params);
        echo "access_token=" . $params['access_token'] . "<br />\n";
        $_SESSION['access_token'] = $params['access_token'];

        $graph_url = "https://graph.facebook.com/me?access_token="
                . $params['access_token'];

        $user = json_decode(file_get_contents($graph_url));
        echo "Hello " . $user->name;

        //////////////////////////////////////////////////
        // gifstory-api request

        require_once 'init.php';

        echo "<pre>";
        echo curl(APIBASE_URL . 'oaccount/social_personize/facebook/desc', array(
                'apikey' => 'cfcd208495d565ef66e7dff9f98764da',
                'token' => ftoken(),
                'otoken' => $params['access_token'],
            ),
            array(
			    CURLOPT_USERAGENT => GIFSTRY_TEST_USER_AGENT,
			),
            $ch);

        echo "\n";
        var_dump(curl_getinfo($ch, CURLINFO_HEADER_OUT));

        //////////////////////////////////////////////////

    } else
    {
        echo("The state does not match. You may be a victim of CSRF.");
    }
} else
{
    echo "error=\"" . $_REQUEST['error'] . "\"<br />\n"
            . "error_reason=\"" . $_REQUEST['error_reason'] . "\"<br />\n"
            . "error_description=\"" . $_REQUEST['error_description'] . "\"<br />\n";
}