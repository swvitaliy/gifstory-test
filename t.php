<?php

if (!isset($_SERVER['argv'][1])) {
    die("expected version\n");
}
if (!isset($_SERVER['argv'][2])) {
    die("expected parameter\n");
}

$version = $_SERVER['argv'][1];
list($dir, $file) = explode('.', $_SERVER['argv'][2]);
$_SERVER['argv'] = array_slice($_SERVER['argv'], 2);
$_SERVER['argc'] -= 2;
include(__DIR__ . '/' . $version . '/' . $dir. '/' . $file . '.php');
