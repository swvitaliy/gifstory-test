<?php

class Profiler {

    private static $marks = array();

    public static function mark($name = NULL, $summary = NULL)
    {
        self::$marks[] = array(
            'name' => $name ?: 'mark#' . count(self::$marks),
            'summary' => $summary ?: '',
            'time' => microtime(true),
        );
    }

    public static function dump()
    {
        return self::$marks;
    }

}

