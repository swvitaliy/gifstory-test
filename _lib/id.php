<?php

/**
 * 
 * @version    1.0
 * @since      1.0
 * @package    SocialAPI
 * @subpackage Modules
 */

class Id {

    public static function encodelen($data, $rand = 0) {
        $i = 0;
        $r = "";
        $size = strlen($data);
        while ($i < $size) {
            $b0 = ord($data[$i]);
            $b0 = ($b0 - 0x30) + $i + (0x41 + ($rand * 10));
            $r = $r . chr($b0);
            $i++;
        }
        return $r;
    }

    public static function decodelen($data, $rand = 0) {
        $i = 0;
        $r = "";
        $size = strlen($data);
        while ($i < $size) {
            $b0 = ord($data[$i]);
            $b0 = ($b0 + 0x30) - $i - (0x41 - ($rand * 10));
            $r = $r . chr($b0);
            $i++;
        }
        return $r;
    }

}