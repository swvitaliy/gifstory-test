<?php

$env = 'web';

$args = $env === 'web' ? $_GET : _argv();

function _argv() {
    $vars = array();
    foreach ($_SERVER['argv'] as $v) {
        $pair = explode('=', $v);
        $vars[$v[0]] = $v[1];
    }
    return $vars;
}

function param($name, $default = NULL) {
    global $env, $args;

    if ($default === NULL) {
        $default = rand_str();
    }

    if ($env === 'web') {
        return isset($_GET[$name]) ? $_GET[$name] : $default;
    } else {
        return isset($args[$name]) ? $args[$name] : $default;
    }
}

function rand_str_len($length, $lowercase = FALSE) {
    $chars = ($lowercase) ? 'qazxswedcvfrtgbnhyujmkiolp1234567890' :
            'qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP';
    $size = strlen($chars) - 1;
    $str = '';
    while ($length--)
        $str .= $chars[rand(0, $size)];
    return $str;
}

function rand_str_range($min, $max, $lowercase = FALSE) {
    return rand_str_len(rand($min, $max), $lowercase);
}


function rand_str($lowercase = FALSE) {
    return rand_str_len(rand(5, 45), $lowercase);
}
