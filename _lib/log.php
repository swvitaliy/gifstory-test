<?php

class Log {

    public static $logs_dir = 'logs/';
    public static $stdout = TRUE;

    /**
     * @var bool|array
     */
    public static $filter = array('INFO', 'ERROR', 'WARNING', 'PROFILING');

    private static $fh = NULL;
    private static $data = array();

    public static function add($type, $message)
    {
        self::$data[] = array(
            'type' => $type,
            'message' => $message,
        );

        if (defined('STDOUT') && self::$stdout) {
            fprintf(STDOUT, "[%s]\t%s\t%s\n", date(DATE_ATOM), $type, $message);
        }
    }

    public static function write()
    {
        if (is_null(self::$fh)) {
            self::$fh = fopen(self::$logs_dir . date('Y-m-d'), 'at');
            if (!self::$fh) {
                return FALSE;
            }
        }

        foreach (self::$data as $v) {
            if (self::$filter === TRUE || in_array($v['type'], self::$filter)) {
                fprintf(self::$fh, "[%s]\t%s\t%s\n", date(DATE_ATOM), $v['type'], $v['message']);
            }
        }
        fclose(self::$fh);
        self::$fh = NULL;
        return TRUE;
    }

}