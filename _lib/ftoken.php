<?php

function ftoken() {
	$fh = fopen('token', 'r');
	if (!$fh) {
		print('file error');
		EXIT(1);
	}
	$ftoken = fread($fh, 1024);
	fclose($fh);
	return $ftoken;
}