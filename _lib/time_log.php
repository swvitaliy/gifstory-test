<?php

register_shutdown_function(function() {
    Log::write();
});

require_once 'init.php';
require_once 'log.php';
require_once 'profiler.php';

$command = implode(' ', array_slice($_SERVER['argv'], 1));

Profiler::mark('start execution', $command);
$output = '';
exec($command, $output);
print $output;
Profiler::mark('stop execution');

Log::add('PROFILING', json_encode(Profiler::dump()));



