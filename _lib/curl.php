<?php

function curl($url, $data = '', $options = array(), &$ch = NULL) {
$ch = curl_init($url);
$options+= array(
CURLOPT_RETURNTRANSFER=>1,
CURLOPT_USERAGENT=>'',
CURLOPT_TIMEOUT=>1000000,
CURLOPT_REFERER=>'',
CURLOPT_FOLLOWLOCATION=>0,
CURLOPT_HEADER=>0,
CURLOPT_NOBODY=>0,
);
if ($data) {
$options+= array(
CURLOPT_POST=>1,
CURLOPT_POSTFIELDS=>$data,
);
}

curl_setopt_array($ch, $options);
curl_setopt($ch, CURLINFO_HEADER_OUT, true);
curl_setopt ($ch, CURLOPT_VERBOSE, 2);
$r = curl_exec($ch);
if ($cn = curl_errno($ch)) {
print("curl error #{$cn} with message \"" . curl_error($ch) . "\"");
EXIT;
}
return $r;
}
